const router = require("express").Router();
var mercadopago = require("mercadopago");

mercadopago.configure({
  sandbox: true,
  access_token:
    "TEST-3003364239035454-051718-cb00e8d7d2f8fc8de8b34490653cddc5-569631570",
  //client_secret:'TEST-acd56276-e345-4283-8cae-b497457db03f',
});

console.log(mercadopago.card_token.schema.properties);

router.post("/rapipago", (req, res) => {
  console.log(req.body);

  mercadopago.payment
    .save(req.body)
    .then((i) => {
      res.send(i);
    })
    .catch(function (error) {
      res.send((error) => res.send(error.message));
    });
});

router.post("/test", (req, res) => {
  // Create card token
  mercadopago.card_token
    .create(req.body)
    .then((i) => res.send(i))
    .catch((err) => res.send({ message: "Nope", err }));
});

router.post("/test/creditcard/makePayment", (req, res) => {
  // Create card token
  mercadopago.payment
    .create(req.body)
    .then((i) => res.send(i))
    .catch((err) => res.send({ message: "Nope", err }));
});

// Search for a costumer
router.post("/test/costumer/search", (req, res) => {
  mercadopago.customers
    .search({ qs: { id: req.body.id } })
    .then((i) => res.send(i.body));
});

// Create customer
router.post("/test/costumer/create", (req, res) => {
  mercadopago.customers
    .create(req.body)
    .then((i) => res.send({ success: true, data: i }))
    .catch((err) => res.send({ success: false, message: err }));
});

router.post("/test/creditcard/create", (req, res) => {
  // Create credit card
  mercadopago.card
    .create({
      customer_id: "570721024-RaK64aqRZfjgt2",
      expiration_month: 11,
      expiration_year: 25,
      first_six_digits: "417006",
      last_four_digits: "8020",
      payment_method: {
        id: "visa",
        name: "APRO",
        payment_type_id: "visa",
        thumbnail: "",
        secure_thumbnail: "",
      },
      security_code: { length: 3, card_location: "back" },
    })
    .then((i) => res.send({ message: "ok", data: i }))
    .catch((i) => res.send({ message: "Error", data: i }));
});

module.exports = router;
