window.Mercadopago.setPublishableKey(
  "TEST-438f8af3-2a09-4738-883d-abe1c72366cf"
);

window.Mercadopago.getIdentificationTypes();

document
  .getElementById("cardNumber")
  .addEventListener("keyup", guessPaymentMethod);
document
  .getElementById("cardNumber")
  .addEventListener("change", guessPaymentMethod);

function guessPaymentMethod(event) {
  let cardnumber = document.getElementById("cardNumber").value;

  if (cardnumber.length >= 6) {
    let bin = cardnumber.substring(0, 6);
    window.Mercadopago.getPaymentMethod(
      {
        bin: bin,
      },
      setPaymentMethod
    );
  }
}

function setPaymentMethod(status, response) {
  if (status == 200) {
    let paymentMethodId = response[0].id;
    let element = document.getElementById("payment_method_id");
    element.value = paymentMethodId;
    getInstallments();
  } else {
    alert(`payment method info error: ${response}`);
  }
}

function getInstallments() {
  window.Mercadopago.getInstallments(
    {
      payment_method_id: document.getElementById("payment_method_id").value,
      amount: parseFloat(document.getElementById("transaction_amount").value),
    },
    function (status, response) {
      if (status == 200) {
        document.getElementById("installments").options.length = 0;
        response[0].payer_costs.forEach((installment) => {
          let opt = document.createElement("option");
          opt.text = installment.recommended_message;
          opt.value = installment.installments;
          document.getElementById("installments").appendChild(opt);
        });
      } else {
        alert(`installments method info error: ${response}`);
      }
    }
  );
}

doSubmit = false;
document.querySelector("#pay").addEventListener("submit", doPay);

function doPay(event) {
  event.preventDefault();
  if (!doSubmit) {
    var $form = document.querySelector("#pay");
    var formData = new FormData($form);

    for (var [key, value] of formData.entries()) {
      console.log(key, value);
    }
    window.Mercadopago.createToken($form, sdkResponseHandler);

    return false;
  }
}

function sdkResponseHandler(status, response) {
  if (status != 200 && status != 201) {
    alert(response.cause[0].description);
  } else {
    var form = document.querySelector("#pay");
    var card = document.createElement("input");
    card.setAttribute("name", "token");
    card.setAttribute("type", "hidden");
    card.setAttribute("value", response.id);
    form.appendChild(card);
    var formData = new FormData(form);
    let data = {};

    for (var [key, value] of formData.entries()) {
      if (key === "email") {
        data = { ...data, payer: { email: value } };
        continue;
      }

      if (key === "transaction_amount") {
        data = { ...data, [key]: parseInt(value) };
        continue;
      }

      if (key === "installments") {
        data = { ...data, [key]: parseInt(value) };
        continue;
      }
      data = { ...data, [key]: value };
    }
    console.log(data);

    fetch("http://localhost:2048/api/payment/post", {
      method: "post",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    })
      .then(function (response) {
        console.log(response);
      })
      .catch((err) => console.log(err));
  }
}
