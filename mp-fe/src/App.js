import React, { Component } from "react";
import { Container } from "react-bootstrap";
import Rapipago from "./paymentMethods/Rapipago";
import CreditCard from "./paymentMethods/CreditCard";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    const script = document.createElement("script");
    script.src = "https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js";
    script.async = true;
    document.body.appendChild(script);
  }

  render() {
    return (
      <Container>
        <div className="m-5 p-3 border border-gray rounded">
          <Rapipago />
        </div>
        <hr />
        <div className="m-5 p-3 border border-gray rounded">
          <CreditCard />
        </div>
      </Container>
    );
  }
}
