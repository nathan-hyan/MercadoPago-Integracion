import React, { Component } from "react";
import { Form, Button, Container } from "react-bootstrap";
import Axios from "axios";


export default class Rapipago extends Component {
  constructor(props) {
    super(props);

    this.state = {
      description: "Tela de araña",
      transaction_amount: 100,

      payment_method_id: "visa",
      payer: {
        email: "test_user_78476227@testuser.com",

        identification: {
          type: "D.N.I.",
          number: "1111111",
        },
      },
      barcodeNumber: "",
    };
  }


  handleSubmit = (e) => {
    console.log("Exec");

    e.preventDefault();

    let {
      description,
      transaction_amount,
      payment_method_id,
      payer,
    } = this.state;

    Axios.post("http://localhost:8000/api/creditcard", {
      description,
      transaction_amount,
      payment_method_id,
      payer,
    })
      .then((res) => {
        this.setState({ barcodeNumber: res.data.response.barcode.content });
      })
      .catch((err) => {
        console.log("ERRRROR" + err);
      });
  };

  handleChange = (e) => {
    let { name, value } = e.target;

    this.setState({ [name]: value });

    if (name === "cardNumber") {
      this.guessPaymentMethod();
    }
  };

  render() {
    return (
      <>
        <h1 className="display-4 text-center">Credit Card Test</h1>
        <hr />
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="description">
            <Form.Label>Descripción</Form.Label>
            <Form.Control
              type="text"
              value={this.state.description}
              name="description"
              onChange={this.handleChange}
            />
          </Form.Group>

          <Form.Group controlId="transaction_amount">
            <Form.Label>Monto a pagar</Form.Label>
            <Form.Control
              type="number"
              value={this.state.transaction_amount}
              name="transaction_amount"
              onChange={this.handleChange}
            />
          </Form.Group>

          <Form.Group controlId="docType">
            <Form.Label>Tipo de documento</Form.Label>
            <Form.Control
              type="number"
              value={this.state.payer.identification.type}
              name="docType"
              onChange={this.handleChange}
            />
          </Form.Group>

          <Form.Group controlId="docNumber">
            <Form.Label>Numero de documento</Form.Label>
            <Form.Control
              type="number"
              value={this.state.payer.identification.number}
              name="docNumber"
              onChange={this.handleChange}
            />
          </Form.Group>

          <Form.Group controlId="email">
            <Form.Label>E-mail</Form.Label>
            <Form.Control
              type="email"
              value={this.state.payer.email}
              name="email"
              onChange={this.handleChange}
            />
          </Form.Group>

          <Button variant="primary" type="submit" className="mb-5">
            Submit
          </Button>
        </Form>

        <code>Código: {this.state.barcodeNumber}</code>
      </>
    );
  }
}
