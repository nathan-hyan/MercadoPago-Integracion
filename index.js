const express = require(`express`);
const app = express();
const cors = require(`cors`);
require(`dotenv`).config();

// Clear console
console.log("\033[2J");

// Middlewares
app.use(express.json());
app.use(cors());

app.use("/api", require("./routes/MercadoPago"));

app.set(`port`, process.env.PORT || 8000);
const main = async () => {
  await app.listen(app.get(`port`));

  console.log(`Servidor iniciado en puerto ${app.get(`port`)}`);
};
main();
